import unittest
import netsquid as ns
from netsquid.qubits.ketstates import h0, h1
import netsquid.qubits.qubitapi as qapi
from netsquid.components.qprocessor import QuantumProcessor
from netsquid.nodes.node import Node
import numpy as np
from netsquid_nv.magic_distributor import NVSingleClickMagicDistributor, NVDoubleClickMagicDistributor
from netsquid_nv.nv_center import NVQuantumProcessor
from netsquid_physlayer.heralded_connection import HeraldedConnection
from netsquid.components.qmemory import QuantumMemory

nv_params = {'carbon_T1': 36000000000000.0,
             'carbon_T2': 1000000000.0,
             'carbon_init_depolar_prob': 0.006000000000000005,
             'carbon_z_rot_depolar_prob': 0.0013333333333333333,
             'coherent_phase': 0.0,
             'ec_gate_depolar_prob': 0.02,
             'electron_T1': 3600000000000.0,
             'electron_T2': 1460000000.0,
             'electron_init_depolar_prob': 0.02,
             'electron_single_qubit_depolar_prob': 0.0,
             'initial_nuclear_phase': 0.0,
             'p_double_exc': 0.06,
             'p_fail_class_corr': 0.0,
             'photon_emission_delay': 3800.0,
             'prob_dark_count': 2.4999999737040923e-08,
             'prob_error_0': 0.050000000000000044,
             'prob_error_1': 0.0050000000000000044,
             'tau_decay': 0.5346179939670399,
             'delta_w': 0.1,
             'std_electron_electron_phase_drift': 0.3490658503988659,
             'visibility': 0.9,
             'num_positions': 2,
             'emission_duration_A': 5800.,
             'emission_duration_B': 5800.,
             'electron_init_duration': 2000.,
             }
lengths_and_speed_of_light = {"length_A": 10,
                              "length_B": 20,
                              "speed_of_light_A": 10000,
                              "speed_of_light_B": 2000000}

NAME_TO_DISTRIBUTOR = {"single": NVSingleClickMagicDistributor,
                       "double": NVDoubleClickMagicDistributor}


class NVNode(Node):
    def __init__(self, name, electron_init_depolar_prob, electron_single_qubit_depolar_prob,
                 prob_error_0, prob_error_1, carbon_init_depolar_prob, carbon_z_rot_depolar_prob,
                 ec_gate_depolar_prob, electron_T1, electron_T2, carbon_T1, carbon_T2, coherent_phase,
                 initial_nuclear_phase, p_double_exc, p_fail_class_corr, photon_emission_delay, prob_dark_count,
                 tau_decay, delta_w, std_electron_electron_phase_drift, visibility, num_positions,
                 use_magical_swap=False, port_names=None, alpha_A=None, alpha_B=None, **kwargs):

        qprocessor = NVQuantumProcessor(
            name="nvprocessor", num_positions=num_positions,
            electron_init_depolar_prob=electron_init_depolar_prob,
            electron_single_qubit_depolar_prob=electron_single_qubit_depolar_prob,
            prob_error_0=prob_error_0,
            prob_error_1=prob_error_1,
            carbon_init_depolar_prob=carbon_init_depolar_prob,
            carbon_z_rot_depolar_prob=carbon_z_rot_depolar_prob,
            ec_gate_depolar_prob=ec_gate_depolar_prob,
            electron_T1=electron_T1,
            electron_T2=electron_T2,
            carbon_T1=carbon_T1,
            carbon_T2=carbon_T2,
            coherent_phase=coherent_phase,
            initial_nuclear_phase=initial_nuclear_phase,
            p_double_exc=p_double_exc,
            p_fail_class_corr=p_fail_class_corr,
            photon_emission_delay=photon_emission_delay,
            prob_dark_count=prob_dark_count,
            tau_decay=tau_decay,
            delta_w=delta_w,
            std_electron_electron_phase_drift=std_electron_electron_phase_drift,
            visibility=visibility,
            alpha_A=alpha_A,
            alpha_B=alpha_B,
            use_magical_swap=use_magical_swap)

        super().__init__(name=name, qmemory=qprocessor, port_names=port_names)


class TestNVSingleClickMagicDistributor(unittest.TestCase):

    def setUp(self):

        # initialize a network of two nodes
        qmemory_a = QuantumProcessor("qmem_a", num_positions=10)
        qmemory_b = QuantumProcessor("qmem_b", num_positions=10)
        self.node_a = Node("A", ID=23, qmemory=qmemory_a)
        self.node_b = Node("B", ID=42, qmemory=qmemory_b)
        self.distance = 25  # length needs to be set for single-click MD when there is no heralded connection

    def test_nuclear_noise_application(self):

        for ket in [h0, h1]:

            # add qubits
            qmemory_a = self.node_a.qmemory
            qmemory_a.put(qapi.create_qubits(1), positions=1)
            qmemory_a.put(qapi.create_qubits(1), positions=2)

            class _NVSingleClickMagicDistributor(NVSingleClickMagicDistributor):

                def _apply_induced_dephasing_noise(self, nuclear_qubits, alpha, delivery):
                    for qubit in nuclear_qubits:
                        qapi.assign_qstate([qubit], ket)

                def _apply_initial_dephasing_noise(self, nuclear_qubits, delivery):
                    pass

            # initialize NVSingleClickMagicDistributor and magically generate a state
            # between the electron spins
            distributor = _NVSingleClickMagicDistributor(nodes=[self.node_a, self.node_b],
                                                         length_A=self.distance, length_B=self.distance)
            electron_pos = 0
            memory_positions = {self.node_a.ID: electron_pos,
                                self.node_b.ID: electron_pos}
            other_parameters = {"length_A": 10,
                                "length_B": 10,
                                "p_loss_length_A": 0,
                                "p_loss_length_B": 0,
                                "p_loss_init_A": 0,
                                "p_loss_init_B": 0,
                                "detector_efficiency": 1}
            distributor.add_delivery(memory_positions=memory_positions,
                                     alpha=0.1,
                                     cycle_time=53,
                                     **other_parameters)
            ns.sim_run()

            # peek to see if the electron spins are now entangled
            [electron_qubit_a] = self.node_a.qmemory.peek(0)
            [electron_qubit_b] = self.node_b.qmemory.peek(0)
            self.assertTrue(electron_qubit_a.qstate == electron_qubit_b.qstate)

            # peek to see if we get the noise was applied indeed
            for qmemory in [self.node_a.qmemory, self.node_b.qmemory]:
                for position in [1, 2]:
                    [nuclear_qubit] = qmemory_a.peek(1)
                    self.assertTrue(np.isclose(
                        qapi.fidelity(nuclear_qubit, ket), 1))


def _setup_nv_magic_distributor(nv_params, lengths_and_speed_of_light, distributor, build_from_connection=True):
    if build_from_connection:
        nodes = [NVNode(name="start_node", **nv_params), NVNode(name="end_node", **nv_params)]
        heralded_connection = HeraldedConnection(name="heralded_connection", **lengths_and_speed_of_light)

        return NAME_TO_DISTRIBUTOR[distributor](nodes=nodes, heralded_connection=heralded_connection)
    else:
        qmemories = [QuantumMemory("start_node"), QuantumMemory("end_node")]
        nodes = [Node(name=f"node_{qmemory.name}", qmemory=qmemory) for qmemory in qmemories]
        return NAME_TO_DISTRIBUTOR[distributor](nodes=nodes, **nv_params, **lengths_and_speed_of_light)


def test_setting_parameters_node():
    for build_from_connection in [True, False]:
        for distributor in ["single", "double"]:
            print(distributor)
            magic_distributor = _setup_nv_magic_distributor(nv_params=nv_params,
                                                            lengths_and_speed_of_light=lengths_and_speed_of_light,
                                                            distributor=distributor,
                                                            build_from_connection=build_from_connection)

            for parameter in ["delta_w", "tau_decay"]:
                assert magic_distributor.fixed_delivery_parameters[0][parameter] == nv_params[parameter]
            if distributor == "single":
                for parameter in ["std_electron_electron_phase_drift", "p_fail_class_corr", "p_double_exc"]:
                    assert magic_distributor.fixed_delivery_parameters[0][parameter] == nv_params[parameter]
            assert magic_distributor.fixed_delivery_parameters[0]["emission_duration_A"] == \
                nv_params["electron_init_duration"] + nv_params["photon_emission_delay"]


def test_cycle_time():
    for build_from_connection in [True, False]:
        for distributor in ["single", "double"]:
            magic_distributor = _setup_nv_magic_distributor(nv_params=nv_params,
                                                            lengths_and_speed_of_light=lengths_and_speed_of_light,
                                                            distributor=distributor,
                                                            build_from_connection=build_from_connection)
            # 15 km divided by the speed of light * 2
            cycle_time = 10 * 1E9 / 10000 * 2 + nv_params["electron_init_duration"] + nv_params["photon_emission_delay"]
            assert cycle_time == magic_distributor.fixed_delivery_parameters[0]["cycle_time"]


if __name__ == "__main__":
    unittest.main()
