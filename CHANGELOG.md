CHANGELOG
=========

2022-08-02 (9.2.1)
------------------
- all parameters that could be passed to `NVSingleClickMagicDistributor` and `NVDoubleClickMagicDistributor` via components can now also be passed through simple parameter passing

2022-04-05 (9.2.0)
------------------
- added `n1e` (number of entanglement generation attempts tolerated before carbon spin dephases) as property of `NVParameterSet`, as well as functions to compute it from `product_tau_decay_delta_w`

2022-01-21 (9.1.0)
------------------
- added `NVDoubleClickMagicDistributor`, with the same state model as the distributor in `netsquid-magic`, but adding induced dephasing
- changes to `docs/Makefile` to move contents to common `Makefile`
- adapted CI to allow for new proxy server
- added deploy command for documentation to `setup.py`

2021-10-29 (9.0.0)
------------------
- `_apply_induced_dephasing_noise` now takes into account the possibility of different nodes having different bright state parameters
- deleted some functionality from `NVSingleClickMagicDistributor` (e.g. computing cycle time, reading parameters), because as of `netsquid-magic` v13.0.0 it is now done in the parent class

2021-09-01 (8.0.0)
------------------
- refactored `nv_state_delivery_model.py` and `state_delivery_sampler_factory.py` to make them compatible with netsquid-magic v12.0.0

2021-08-04 (7.0.2)
------------------
- fixed passing of faulty measurement operators in the initialization of `NVQuantumProcessor` and added respective test to `test_nv_center.py`

2021-07-06 (7.0.1)
------------------
- `add_delivery` method of NVSingleClickMagicDistributor now gets `alpha` and `cycle_time` from fixed delivery parameters if they are not explicitly set in delivery request

2021-07-01 (7.0.0)
------------------
- updated maintainer
- added circuits for moving states and performing bell state measurements in NV centers
- created NVCenter class subclassing from NetSquid's QuantumProcessor
- updated NVMagicDistributor to make it read parameters from physical connections and nodes
- updated requirements to netsquid-magic v11.0.1 in order to take important bugfix into account

2021-06-02 (6.0.0)
------------------
- renamed NVMagicDistributor to NVSingleClickMagicDistributor
- removed deprecated code regarding the magic distributor obtaining parameters from the photon emission noise model (we might bring this functionality back later again)
- name change of the interferometric drift parameters
- After updates in netsquid-magic:
    - fix tests and example 
    - update requirements
    - refactor parameter names and extend from SingleClickMD
- update to netsquid-magic v10.0.0


2020-12-03 (5.0.2)
------------------
- updated dependency to netsquid-magic v.7.0.0
- removed e-mail addresses from contributors who are not maintainers
- removed broken example `example_link_layer_NV.py` (corresponding example in netsquid-magic was also taken out of the pipeline)

2020-10-20 (5.0.1)
------------------
- changed requirement `dataclasses>=0.7,<1.0.0` to `dataclasses>0.0.0,<1.0.0` because the former does not work on some systems

2020-10-19 (5.0.0)
------------------
- updated with NetSQUID v0.10 and netsquid-magic v6.0.1 and netsquid-physlayer v4.0.2:
    - in `NVMagicDistributor`, made the keys of `electron_memory_positions` the quantum memory instead of a node ID
    - adjusted the `NVMagicDistributor` to the latest changes in `MagicDistributor` in the `netsquid-magic` snippet
    - removed NVCommunicationDevice and MakePairsProtocols after their removal in netsquid-physlayer
- remove todos from README

2020-09-22 (4.2.0)
------------------
- added "near term parameters" for Delft, as estimated in 2020
- fixed incorrect computation of gate errors in "Delft 2019" NV parameter set

2020-09-16 (4.1.0)
------------------
- Move code for NV photon noise from physlayer to here

2020-04-29 (4.0.5)
------------------
- updated README with maintainer of this snippet

2020-03-25 (4.0.4)
-----------------
- Allowing for netsquid 0.8 and working with latest netsquid-magic

2020-03-06 (4.0.3)
-----------------
- fixed bug in `NVMagicDistributor`: induced nuclear dephasing was also applied to electron spin
- added (partial) test for `NVMagicDistributor`
- improved upon incorrect functions for conversion between parameters describing induced nuclear dephasing in `~netsquid_nv.nv_parameter_set`
- added tests to verify that those functions are each others' inverses

2020-01-22 (4.0.2)
------------------
- Allow for netsquid 0.7

2020-01-20 (4.0.1)
------------------
- Allow for netsquid-magic v0.1.1

2019-12-03
----------
- file `parameter_set.py` was moved to new snippet NetSquid-Simulationtools
- updated `NVParameterSet` to be in compatible with added parameter 'units' to 'ParameterSet'
- renamed parameter `total_detection_eff` and removed `magical_swap_gate_depolar_prob` in `NVParameterSet`
- cleaned up `delft_nv_2019.py` and `delft_nv_2017.py`
- added optimistic parameters for NV in 2019, see file `delft_nv_2019_optimistic.py`

2019-11-12 (4.0.0)
------------------
- Added `parameter_set.py` with class `ParameterSet`, an abstract base class for storing parameters
- Added `NVParameterSet`, a base class which specifies the parameters that any set of parameters for NV centers should have
- Made `delft_nvs/delft_nv_2019.py` and `delft_nvs/delft_nv_2017.py` adhere to the format, i.e. made their contents in classes which inherit from `NVParameterSet`


2019-11-11 (3.1.1)
------------------
Renamed 
- Module/package names made snake case, e.g. `Delft_NVs` -> `delft_nvs`.
- Add an init.py to `delft_nvs` such that we can import it.
- Fixed linting errors
- Added README file in `delft_nvs` folder


2019-10-31
----------
Renamed private attribute of `MagicDistributor` called `_qmemorys` to `_qmemories` to be compatible with change in `NetSquid-Magic` snippet.


2019-10-04
----------

* made explicit that there is a difference between `coherent_phase` and `std_phase_interferometric_drift` (previously called `std_total_phase`)
  * added a method for initial dephasing of the carbon memories during entanglement attempt (i.e. dephasing independent of the number of attempts)
  * updated examples
  * added parameters for the Delft-2017 case


2019-06-28
----------

- Created this snippet
