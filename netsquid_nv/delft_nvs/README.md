This folder contains a large number of parameters concerning the emission of a
photon by an NV-center, its intereference in a beam splitter and its detection
in the midpoint.

The files in this folder are named `delft_nv_<year>.py` and contain parameters
for NV centers up to the year `<year>`. In addition, there are a few auxillary
files for computing parameter values from data (e.g. `compute_CXDirection_noise_parameter.py`,
where the noise parameter for a `CXDirection` gate is computed from
the fidelity that the gate should output).
