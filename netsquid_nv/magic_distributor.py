from netsquid.components.models.qerrormodels import QuantumErrorModel
from netsquid.qubits.qubitapi import apply_pauli_noise
from netsquid_physlayer.heralded_connection import HeraldedConnection

from netsquid_nv.state_delivery_sampler_factory import NVStateDeliverySamplerFactory
import netsquid_nv.nv_state_delivery_model as nv_model
from netsquid_magic.magic_distributor import SingleClickMagicDistributor, DoubleClickMagicDistributor


class MultiTimesDephasingNoiseModel(QuantumErrorModel):
    """
    A noise model that effectively applies multiple applications
    of dephasing in Z with a fixed probability.
    """

    def __init__(self, prob_of_dephasing):
        self.prob_of_dephasing = prob_of_dephasing

    def noise_operation(self, qubits, number_of_applications):
        # The probability of not-dephasing after a single
        # application of dephasing ...
        prob_single = 1. - self.prob_of_dephasing

        # ... and after multiple applications:
        prob_multiple = (1. + (2. * prob_single - 1) ** number_of_applications) / 2.

        # apply dephasing in Z
        for qubit in qubits:
            apply_pauli_noise(qubit, (prob_multiple, 0., 0., 1. - prob_multiple))


class NVSingleClickMagicDistributor(SingleClickMagicDistributor):
    r"""Magic distributor, tailored to entanglement generation using the
    electron spins in nitrogen-vacancy centers in diamond. Ensures that
    after entanglement generation which took `n` attempts up to and including
    success, a noise channel is applied to the nuclear spins which is equivalent
    to a dephasing noise channel which is applied `n` times to the nuclear spins.
    In addition, an initial amount of dephasing is applied, independent of the number
    of attempts until success. The probabilities of these individual dephasing channels
    are computed using methods in `netsquid_nv.nv_state_delivery_model` and parameters
    to these methods are extracted from the nodes and components specified to the
    NVSingleClickMagicDistributor upon initialization.

    Parameters
    ----------
    nodes : list of :obj:`~netsquid.nodes.node.Node`
        List of nodes for which entanglement can be produced.
    electron_memory_positions : dict str: int
        Dictionary which maps qmemory names to memory position at the node's quantum memory.
        If equals "default", then it is assumed that for
        all nodes, the electron memory position is 0.
    heralded_connection : :obj:`~netsquid_physlayer.heraldedGeneration.HeraldedConnection` or None (optional)
        Heralded connection which magic is supposed to replace. Used to extract delivery parameters.
        Parameters extracted from the connection are overwritten by the other parameters given in this constructor
        (if they are not None), a warning is given when this happens.
    length_A: float or None (optional)
        Length [km] of fiber on "A" side of heralded connection. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        Cannot be None if heralded_connection is None.
    length_B: float or None (optional)
        Length [km] of fiber on "B" side of heralded connection. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        Cannot be None if heralded_connection is None.
    p_loss_init_A: float or None (optional)
        Probability that photons are lost when entering connection on "A" side.
        Includes collection efficiency of emission from memory. Defaults to None.
        If None, the parameter is obtained from p_loss_init_A of heralded_connection and collection_efficiency
        property of memory on side "A".
        If this fails, the default value 0 will be used.
    p_loss_length_A: float or None (optional)
        Attenuation coefficient [dB/km] of fiber between "A" and midpoint. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        If this fails, the default value 0.25 will be used.
    speed_of_light_A: float or None (optional)
        Speed of light [km/s] in fiber on "A" side of heralded connection. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        If this fails, the default value 200,000 will be used.
    p_loss_init_B: float or None (optional)
        Probability that photons are lost when entering connection on "B" side.
        Includes collection efficiency of emission from memory. Defaults to None.
        If None, the parameter is obtained from heralded_connection and collection_efficiency
        property of memory on side "B".
        If this fails, the default value 0 will be used.
    p_loss_length_B: float or None (optional)
        Attenuation coefficient [dB/km] of fiber between "B" and midpoint. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        If this fails, the default value 0.25 will be used.
    speed_of_light_B: float or None (optional)
        Speed of light [km/s] in fiber on "B" side of heralded connection. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
       If this fails, the default value 200,000 will be used.
    dark_count_probability: float or None (optional)
        Dark-count probability per detection. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        If this fails, the default value 0 will be used.
    detector_efficiency: float or None (optional)
        Probability that the presence of a photon leads to a detection event. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        If this fails, the default value 1 will be used.
    visibility: float or None (optional)
        Hong-Ou-Mandel visibility of photons that are being interfered (measure of photon indistinguishability).
        Defaults to None. If None, the parameter is obtained from heralded_connection.
        If this fails, the default value 1 will be used.
    num_resolving : bool or None (optional)
        Determines whether photon-number-resolving detectors are used for the Bell-state measurement. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        If this fails, the default value False will be used.
    std_electron_electron_phase_drift : float or None (optional)
        Standard deviation of the phase of the produced state caused by the drift on the interferometer (radians).
        If this fails, the default value 0 will be used.
    coherent_phase : float or None (optional)
        Phase between components of generated Bell state (radians).
        If this fails, the default value 0 will be used.
    delta_w : float or None (optional)
        Coupling strength. Note: usually specified as :math:`XXX / (2 \pi) ` kiloHertz,
        but this input parameter should be specified in Hz, without the factor :math:`1/(2\pi)`.
    tau_decay : float or None (optional)
        Decay constant (seconds).
    p_double_exc : float or None (optional)
        Probability of two photons being excited instead of one.
        If this fails, the default value 0 will be used.
    p_fail_class_corr : float or None (optional)
        Probability of correlation between spin and photon state failing.
        If this fails, the default value 0 will be used.
    """

    DEFAULT_ELECTRON_POSITION = 0
    SIDES = ["A", "B"]  # Names of ports of heralded connection, used to keep the two sides apart
    DEFAULT_VALUES = {"p_loss_init_A": 0, "p_loss_length_A": .25, "p_loss_init_B": 0, "p_loss_length_B": .25,
                      "detector_efficiency": 1, "dark_count_probability": 0, "visibility": 1, "p_fail_class_corr": 0,
                      "coherent_phase": 0, "p_double_exc": 0, "std_electron_electron_phase_drift": 0,
                      "emission_duration_A": 0, "emission_duration_B": 0, "tau_decay": 0, "delta_w": 0,
                      "speed_of_light_A": 200000, "speed_of_light_B": 200000, "num_resolving": False}

    def __init__(self, nodes, electron_memory_positions="default", heralded_connection=None, length_A=None,
                 length_B=None, p_loss_init_A=None, p_loss_init_B=None, p_loss_length_A=None, p_loss_length_B=None,
                 speed_of_light_A=None, speed_of_light_B=None, detector_efficiency=None, dark_count_probability=None,
                 visibility=None, p_fail_class_corr=None, coherent_phase=None, p_double_exc=None, tau_decay=None,
                 delta_w=None, std_electron_electron_phase_drift=None, emission_duration_A=None,
                 emission_duration_B=None, **kwargs):
        if heralded_connection is not None and not isinstance(heralded_connection, HeraldedConnection):
            raise ValueError("Magic distributor must read properties from a HeraldedConnection")

        kwargs.update({"length_A": length_A, "length_B": length_B, "p_loss_init_A": p_loss_init_A,
                       "p_loss_length_A": p_loss_length_A, "p_loss_init_B": p_loss_init_B,
                       "p_loss_length_B": p_loss_length_B, "detector_efficiency": detector_efficiency,
                       "dark_count_probability": dark_count_probability, "visibility": visibility,
                       "p_fail_class_corr": p_fail_class_corr, "coherent_phase": coherent_phase,
                       "speed_of_light_A": speed_of_light_A, "speed_of_light_B": speed_of_light_B,
                       "emission_duration_A": emission_duration_A, "emission_duration_B": emission_duration_B,
                       "p_double_exc": p_double_exc, "tau_decay": tau_decay, "delta_w": delta_w,
                       "std_electron_electron_phase_drift": std_electron_electron_phase_drift})
        kwargs_not_none = {key: value for (key, value) in kwargs.items() if value is not None}

        super().__init__(nodes=nodes, heralded_connection=heralded_connection, **kwargs_not_none)

        # overwrite the state delivery sampler factory from SingleClickMagicDistributor
        self.delivery_sampler_factory = [NVStateDeliverySamplerFactory()]

        if len(set([node.ID for node in nodes])) != len(nodes):
            raise ValueError("IDs of nodes are not unique")

        if electron_memory_positions == "default":
            electron_memory_positions = \
                {node.qmemory: NVSingleClickMagicDistributor.DEFAULT_ELECTRON_POSITION for node in nodes}

        for pos in electron_memory_positions.values():
            if not isinstance(pos, int):
                raise TypeError("Memory position should be of type int")
        self._electron_memory_positions = electron_memory_positions

    def _read_params_from_nodes_or_component(self):
        delivery_parameters = super()._read_params_from_nodes_or_component()
        delivery_parameters.update(self._read_nv_params_from_node())

        return delivery_parameters

    def _read_nv_params_from_node(self):
        nv_params = {}
        for parameter in ["std_electron_electron_phase_drift", "p_double_exc", "delta_w",
                          "tau_decay", "p_fail_class_corr"]:
            try:
                params_temp = [node.qmemory.properties[parameter] for node in self.nodes[0]]
                if params_temp[0] != params_temp[1]:
                    raise NotImplementedError("NV Centers must have same {}.".format(parameter))
                nv_params[parameter] = params_temp[0]
            except KeyError:
                pass

        return nv_params

    def _get_node_by_id(self, node_id):
        # The attribute `nodes` contains lists of nodes.
        # If this MagicDistributor is not merged, then it is
        # a list of a single element, e.g. [[node_A, node_B]]
        # if the MagicDistributor serves two nodes.
        # If it has been merged, and contains e.g. three initial
        # MagicDistributors, then it contains three lists of nodes,
        # for example [[node_A, node_B], [node_A, node_C], [node_B, node_D]].
        # We flatten this list because all we want to obtain is the
        # Node object given its identifier.
        all_nodes = [node for node_list in self.nodes for node in node_list]
        for node in all_nodes:
            if node_id == node.ID:
                return node
        raise ValueError("Node ID {} unknown".format(node_id))

    def add_delivery(self, memory_positions, alpha=None, alpha_A=None, alpha_B=None, cycle_time=None, **kwargs):
        """
        Called by only one of the nodes. It is possible to set the bright state parameter uniformly for both nodes
        using `alpha`, or differently for the node on each side using `alpha_A` and `alpha_B`. Note that if both
        arguments are used, a warning will be raised and `alpha` will overwrite `alpha_A` and `alpha_B`.

        Parameters
        ----------
        memory_positions : dict
            Keys = node IDs and values = memory positions
        alpha : float or None (optional)
            Bright state parameter. Will be used for nodes on both sides of the connection.
        alpha_A : float or None (optional)
            Bright state parameter. Will be used for node on side "A" of the connection.
        alpha_B : float or None (optional)
            Bright state parameter. Will be used for node on side "B" of the connection.
        cycle_time : float or None (optional)
            Duration [ns] of each round of entanglement distribution.
        kwargs :
            additional args that will be passed on to
            :obj:`~netsquid_magic.state_delivery_sampler.DeliverySampler`.sample

        Returns
        -------
        :obj:`~pydynaa.core.Event`
            One of the scheduled events (there is a different event for each node, but only one of them is returned).
            This event can be used subsequently in other methods such as :meth:`~MagicDistributor.peek_delivery`
            and :meth:`~MagicDistributor.abort_delivery`
        """
        for node_id, memory_position in memory_positions.items():
            node = self._get_node_by_id(node_id)
            qmemory = node.qmemory
            if memory_position != self._electron_memory_positions[qmemory]:
                raise ValueError("NV can only generate entanglement with the electron spins")
        return super().add_delivery(memory_positions=memory_positions, alpha=alpha, alpha_A=alpha_A, alpha_B=alpha_B,
                                    cycle_time=cycle_time, **kwargs)

    def _get_nuclear_qubits(self, qmemory):
        """Get all nuclear spin qubits at a node.

        Parameters
        ----------
        qmemory : :obj:`~netsquid.components.qmemory.QuantumMemory`

        Returns
        -------
        list of :obj:`~netsquid.qubits.qubit.Qubit`
        """
        electron_memory_position = self._electron_memory_positions[qmemory]

        # get memory positions at which nuclear qubits live
        all_memory_positions = set(range(qmemory.num_positions))
        used_positions = set(all_memory_positions) - set(qmemory.unused_positions)
        nuclear_positions = used_positions - set([electron_memory_position])

        # extract nuclear qubits from the memory positions
        nuclear_qubits = []
        for position in nuclear_positions:
            qubits = qmemory.peek(position)
            assert(len(qubits) == 1)
            qubit = qubits[0]
            nuclear_qubits.append(qubit)

        return nuclear_qubits

    @staticmethod
    def _get_number_of_attempts(delivery):
        """Get how many attempts were performed before a success was obtained.

        Parameters
        ----------
        delivery : named tuple

        """
        try:
            cycle_time = delivery.parameters["cycle_time"]
        except KeyError:
            raise ValueError("Received no parameter 'cycle_time', neither when the magic" +
                             "distributor was initialized or when 'add_delivery' was called.")
        return int(delivery.sample.delivery_duration / cycle_time)

    def _apply_noise(self, delivery, quantum_memory, positions):
        """Applies initial dephasing and induced dephasing noise to carbon qubits.

        `positions` argument is ignored, existing only for compatibility with parent class.

        Parameters
        ----------
        delivery : named tuple
            Contains information on memory positions where to place state, ports and relevant parameters.
        quantum_memory : :obj:`~netsquid.components.qmemory.QuantumMemory`
            Quantum memory on whose qubits noise should be applied.
        positions : list
            List of positions. Not used.
        """

        # get all qubits that are not the involved memory positions_of_connections
        nuclear_qubits = self._get_nuclear_qubits(qmemory=quantum_memory)

        # determine which bright state parameter to use for computing dephasing noise
        node_to_apply_noise_id = "A" if quantum_memory.supercomponent == self.node_dict["A"] else "B"
        alpha_for_noise = delivery.parameters["alpha_" + node_to_apply_noise_id]

        # apply dephasing noise to the nuclear spins
        self._apply_initial_dephasing_noise(nuclear_qubits=nuclear_qubits,
                                            delivery=delivery)
        self._apply_induced_dephasing_noise(nuclear_qubits=nuclear_qubits,
                                            alpha=alpha_for_noise,
                                            delivery=delivery)

    def _apply_induced_dephasing_noise(self, nuclear_qubits, alpha, delivery):
        """Applies induced dephasing noise to carbon qubits.

        Parameters
        ----------
        nuclear_qubits : list
            List of qubits to which noise should be applied.
        alpha : float
            Bright state parameter to be considered when computing noise.
        delivery : named tuple
            Contains information on memory positions where to place state, ports and relevant parameters.
        """
        p_deph = nv_model.prob_nuclear_dephasing_during_entgen_attempt(alpha=alpha,
                                                                       delta_w=delivery.parameters["delta_w"],
                                                                       tau_decay=delivery.parameters["tau_decay"])
        error_model = MultiTimesDephasingNoiseModel(prob_of_dephasing=p_deph)
        number_of_attempts = NVSingleClickMagicDistributor._get_number_of_attempts(delivery)
        error_model.noise_operation(
            qubits=nuclear_qubits,
            number_of_applications=number_of_attempts)

    def _apply_initial_dephasing_noise(self, nuclear_qubits, delivery):
        """Applies initial dephasing noise to carbon qubits.

        Parameters
        ----------
        nuclear_qubits : list
            List of qubits to which noise should be applied.
        delivery : named tuple
            Contains information on memory positions where to place state, ports and relevant parameters.
        """
        p_initial_deph = nv_model.prob_of_initial_nuclear_dephasing(**delivery.parameters)
        error_model = MultiTimesDephasingNoiseModel(prob_of_dephasing=p_initial_deph)
        error_model.noise_operation(
            qubits=nuclear_qubits,
            number_of_applications=1)


class NVDoubleClickMagicDistributor(DoubleClickMagicDistributor):
    """Magic distributor for entanglement generation using the electron spins in nitrogen-vacancy centers in diamond.

    Implements similar functionality as `NVSingleClickMagicDistributor`, with two major differences:

     - The state distributed is one obtained assuming a double-click entanglement generation protocol (see parent class
     for details).
     - The dephasing noise, handled by the `_apply_initial_dephasing_noise` and `_apply_induced_dephasing_noise`
     methods, is applied twice. This is because in a double-click entaglement generation protocol the electron spin must
     be initialized twice per entanglement generation attempt, as opposed to once in a single-click entanglement
     generation protocol.

    Parameters
    ----------
    nodes : list of :obj:`~netsquid.nodes.node.Node`
        List of nodes for which entanglement can be produced.
    electron_memory_positions : dict str: int
        Dictionary which maps qmemory names to memory position at the node's quantum memory.
        If equals "default", then it is assumed that for
        all nodes, the electron memory position is 0.
    heralded_connection : :obj:`~netsquid_physlayer.heraldedGeneration.HeraldedConnection` or None (optional)
        Heralded connection which magic is supposed to replace. Used to extract delivery parameters.
        Parameters extracted from the connection are overwritten by the other parameters given in this constructor
        (if they are not None), a warning is given when this happens.
    length_A: float or None (optional)
        Length [km] of fiber on "A" side of heralded connection. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        Cannot be None if heralded_connection is None.
    length_B: float or None (optional)
        Length [km] of fiber on "B" side of heralded connection. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        Cannot be None if heralded_connection is None.
    p_loss_init_A: float or None (optional)
        Probability that photons are lost when entering connection on "A" side.
        Includes collection efficiency of emission from memory. Defaults to None.
        If None, the parameter is obtained from p_loss_init_A of heralded_connection and collection_efficiency
        property of memory on side "A".
        If this fails, the default value 0 will be used.
    p_loss_length_A: float or None (optional)
        Attenuation coefficient [dB/km] of fiber between "A" and midpoint. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        If this fails, the default value 0.25 will be used.
    speed_of_light_A: float or None (optional)
        Speed of light [km/s] in fiber on "A" side of heralded connection. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        If this fails, the default value 200,000 will be used.
    p_loss_init_B: float or None (optional)
        Probability that photons are lost when entering connection on "B" side.
        Includes collection efficiency of emission from memory. Defaults to None.
        If None, the parameter is obtained from heralded_connection and collection_efficiency
        property of memory on side "B".
        If this fails, the default value 0 will be used.
    p_loss_length_B: float or None (optional)
        Attenuation coefficient [dB/km] of fiber between "B" and midpoint. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        If this fails, the default value 0.25 will be used.
    speed_of_light_B: float or None (optional)
        Speed of light [km/s] in fiber on "B" side of heralded connection. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
       If this fails, the default value 200,000 will be used.
    dark_count_probability: float or None (optional)
        Dark-count probability per detection. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        If this fails, the default value 0 will be used.
    detector_efficiency: float or None (optional)
        Probability that the presence of a photon leads to a detection event. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        If this fails, the default value 1 will be used.
    visibility: float or None (optional)
        Hong-Ou-Mandel visibility of photons that are being interfered (measure of photon indistinguishability).
        Defaults to None. If None, the parameter is obtained from heralded_connection.
        If this fails, the default value 1 will be used.
    num_resolving : bool or None (optional)
        Determines whether photon-number-resolving detectors are used for the Bell-state measurement. Defaults to None.
        If None, the parameter is obtained from heralded_connection.
        If this fails, the default value False will be used.
    emission_fidelity_A : float or None (optional)
        Fidelity of state shared between photon and memory qubit on "A" side to
        :meth:`~netsquid.qubits.ketstates.BellIndex.PHI_PLUS` Bell state directly after emission. Defaults to None.
        If None, an attempt is made to obtain "emission_fidelity" from the properties of the memory on the "A" side.
        If this fails, the default value 1 will be used.
    emission_fidelity_B : float or None (optional)
        Fidelity of state shared between photon and memory qubit on "B" side to
        :meth:`~netsquid.qubits.ketstates.BellIndex.PHI_PLUS` Bell state directly after emission. Defaults to None.
        If None, an attempt is made to obtain "emission_fidelity" from the properties of the memory on the "B" side.
        If this fails, the default value 1 will be used.
    emission_duration_A : float or None (optional)
        Time in nanoseconds it takes the memory on the "A" side to emit a photon that is entangled with a memory qubit.
        Defaults to None. If None, an attempt is made to get the instruction durations of
        :obj:`~netsquid.components.instructions.INSTR_INIT` and :obj:`~netsquid.components.instructions.INSTR_EMIT`
        on the memory on the "A" side. The sum of these durations is used as emission duration.
        If this fails, the default value 0 will be used.
    emission_duration_B : float or None (optional)
        Time in nanoseconds it takes the memory on the "B" side to emit a photon that is entangled with a memory qubit.
        Defaults to None. If None, an attempt is made to get the instruction durations of
        :obj:`~netsquid.components.instructions.INSTR_INIT` and :obj:`~netsquid.components.instructions.INSTR_EMIT`
        on the memory on the "B" side. The sum of these durations is used as emission duration.
        If this fails, the default value 0 will be used.

    """

    DEFAULT_ELECTRON_POSITION = 0
    SIDES = ["A", "B"]  # Names of ports of heralded connection, used to keep the two sides apart
    DEFAULT_VALUES = {"p_loss_init_A": 0, "p_loss_length_A": .25, "p_loss_init_B": 0, "p_loss_length_B": .25,
                      "detector_efficiency": 1, "dark_count_probability": 0, "visibility": 1,
                      "speed_of_light_A": 200000, "speed_of_light_B": 200000, "num_resolving": False,
                      "emission_fidelity_A": 1., "emission_fidelity_B": 1., "emission_duration_A": 0.,
                      "tau_decay": 0, "delta_w": 0, "emission_duration_B": 0.}

    def __init__(self, nodes, electron_memory_positions="default", heralded_connection=None, length_A=None,
                 length_B=None, p_loss_init_A=None, p_loss_init_B=None, p_loss_length_A=None, p_loss_length_B=None,
                 speed_of_light_A=None, speed_of_light_B=None, detector_efficiency=None, dark_count_probability=None,
                 visibility=None, num_resolving=False, emission_fidelity_A=None, emission_fidelity_B=None,
                 tau_decay=None, delta_w=None, emission_duration_A=None, emission_duration_B=None, **kwargs):
        if heralded_connection is not None and not isinstance(heralded_connection, HeraldedConnection):
            raise ValueError("Magic distributor must read properties from a HeraldedConnection")

        kwargs.update({"length_A": length_A, "length_B": length_B, "p_loss_init_A": p_loss_init_A,
                       "p_loss_length_A": p_loss_length_A, "p_loss_init_B": p_loss_init_B,
                       "p_loss_length_B": p_loss_length_B, "detector_efficiency": detector_efficiency,
                       "dark_count_probability": dark_count_probability, "visibility": visibility,
                       "speed_of_light_A": speed_of_light_A, "speed_of_light_B": speed_of_light_B,
                       "tau_decay": tau_decay, "delta_w": delta_w, "num_resolving": num_resolving,
                       "emission_fidelity_A": emission_fidelity_A, "emission_fidelity_B": emission_fidelity_B,
                       "emission_duration_A": emission_duration_A, "emission_duration_B": emission_duration_B})
        kwargs_not_none = {key: value for (key, value) in kwargs.items() if value is not None}

        super().__init__(nodes=nodes, heralded_connection=heralded_connection, **kwargs_not_none)

        if len(set([node.ID for node in nodes])) != len(nodes):
            raise ValueError("IDs of nodes are not unique")

        if electron_memory_positions == "default":
            electron_memory_positions = \
                {node.qmemory: NVDoubleClickMagicDistributor.DEFAULT_ELECTRON_POSITION for node in nodes}

        for pos in electron_memory_positions.values():
            if not isinstance(pos, int):
                raise TypeError("Memory position should be of type int")
        self._electron_memory_positions = electron_memory_positions

    def _apply_noise(self, delivery, quantum_memory, positions):
        """Applies initial dephasing and induced dephasing noise to carbon qubits.

        `positions` argument is ignored, existing only for compatibility with parent class. Each form of noise is
        applied twice, because the double-click entanglement generation protocol requires two rounds of excitation.

        The bright state parameter is hardcoded at 0.5, because that is the value used in the double-click entanglement
        generation protocol.

        Parameters
        ----------
        delivery : named tuple
            Contains information on memory positions where to place state, ports and relevant parameters.
        quantum_memory : :obj:`~netsquid.components.qmemory.QuantumMemory`
            Quantum memory on whose qubits noise should be applied.
        positions : list
            List of positions. Not used.
        """

        # get all qubits that are not the involved memory positions_of_connections
        nuclear_qubits = NVSingleClickMagicDistributor._get_nuclear_qubits(self, qmemory=quantum_memory)
        alpha_for_noise = 0.5

        # apply dephasing noise to the nuclear spins
        NVSingleClickMagicDistributor._apply_initial_dephasing_noise(self, nuclear_qubits=nuclear_qubits,
                                                                     delivery=delivery)
        NVSingleClickMagicDistributor._apply_induced_dephasing_noise(self, nuclear_qubits=nuclear_qubits,
                                                                     alpha=alpha_for_noise,
                                                                     delivery=delivery)
        NVSingleClickMagicDistributor._apply_initial_dephasing_noise(self, nuclear_qubits=nuclear_qubits,
                                                                     delivery=delivery)
        NVSingleClickMagicDistributor._apply_induced_dephasing_noise(self, nuclear_qubits=nuclear_qubits,
                                                                     alpha=alpha_for_noise,
                                                                     delivery=delivery)

    def _read_params_from_nodes_or_component(self):
        delivery_parameters = super()._read_params_from_nodes_or_component()
        delivery_parameters.update(self._read_nv_params_from_node())

        return delivery_parameters

    def _read_nv_params_from_node(self):
        nv_params = {}
        for parameter in ["delta_w", "tau_decay"]:
            try:
                params_temp = [node.qmemory.properties[parameter] for node in self.nodes[0]]
                if params_temp[0] != params_temp[1]:
                    raise NotImplementedError("NV Centers must have same {}.".format(parameter))
                nv_params[parameter] = params_temp[0]
            except KeyError:
                pass

        return nv_params
