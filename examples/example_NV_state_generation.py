"""
In this example, we simulate the benchmarking experiment from
"Deterministic delivery of remote entanglement" using
magically generated entanglement.
"""
from netsquid_nv.state_delivery_sampler_factory import NVStateDeliverySamplerFactory
from netsquid_nv.delft_nvs.delft_nv_2019_optimistic import NVParameterSet2019Optimistic
import numpy as np
import matplotlib.pyplot as plt


def main(no_output=False):
    gss_factory = NVStateDeliverySamplerFactory()

    rates = []  # mean rates
    sem_rates = []   # standard errors of the mean of the rate
    alphas = list(np.linspace(0.1, 0.5, 10))

    # Perform sampling
    for alpha in alphas:
        internode_distance = 0.002
        current_params = NVParameterSet2019Optimistic()
        cycle_time = current_params.photon_emission_delay + \
            internode_distance / current_params.c * 10 ** 9
        other_parameters = {"length_A": 10,
                            "length_B": 10,
                            "p_loss_length_A": 0,
                            "p_loss_length_B": 0,
                            "p_loss_init_A": 0,
                            "p_loss_init_B": 0,
                            "detector_efficiency": 1}
        gss = gss_factory.create_state_delivery_sampler(
            **current_params.to_dict(),
            p_det=current_params.to_dict()["prob_detect_excl_transmission_no_conversion_no_cavities"],
            cycle_time=cycle_time,
            alpha_A=alpha,
            alpha_B=alpha,
            dark_count_probability=current_params.to_dict()["prob_dark_count"],
            **other_parameters)

        number_of_samples = 1000

        # sampling
        latencies = [gss.sample()[1] for __ in range(number_of_samples)]

        # Computing mean and standard deviation of the mean
        rates.append(1. / np.mean(latencies) * 10 ** 9)
        std = np.std(latencies) * 10 ** (-9) / (np.mean(latencies) * 10 ** (-9)) ** 2
        sem_rates.append(std / np.sqrt(number_of_samples))

    # Plotting
    if not no_output:
        plt.title(
            "Simulating fig.2 from 'Deterministic delivery of remote entanglement'" +
            "\n(internode distance: 2 m)")
        plt.errorbar(alphas, rates, yerr=sem_rates, capsize=4)
        plt.xlabel(r"Bright-state parameter ($\alpha$)")
        plt.ylabel("Entanglement generation rate (Hz)")
        plt.show()

    return True


if __name__ == "__main__":
    main(no_output=False)
